import React from "react";
import { connect } from "react-redux";
import cx from "classnames";
import { toggleTodo, removeTodo } from "../redux/actions";

const Todo = ({ todo, toggleTodo, removeTodo }) => (
  <li className="todo-item" >
    <div onClick={() => toggleTodo(todo.id)}>
      {todo && todo.completed ? "👌" : "👋"}{" "}
      <span
        className={cx(
          "todo-item__text",
          todo && todo.completed && "todo-item__text--completed"
        )}
      >
        {todo.content}
      </span>
    </div>
<button onClick={()=>removeTodo(todo.id)}>X</button>
  </li>
);

// export default Todo;
export default connect(
  null,
  { toggleTodo, removeTodo }
)(Todo);
