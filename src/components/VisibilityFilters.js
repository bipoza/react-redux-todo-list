import React from "react";
import cx from "classnames";
import { connect } from "react-redux";
import { setFilter } from "../redux/actions";
import { VISIBILITY_FILTERS } from "../constants";

const VisibilityFilters = ({ activeFilter, setFilter, todo_items }) => {
  return (
    <div className="visibility-filters">
      {
        todo_items ?
          Object.keys(VISIBILITY_FILTERS).map(filterKey => {
            const currentFilter = VISIBILITY_FILTERS[filterKey];
            return (
              <span
                key={`visibility-filter-${currentFilter}`}
                className={cx(
                  "filter",
                  currentFilter === activeFilter && "filter--active"
                )}
                onClick={() => {
                  setFilter(currentFilter);
                }}
              >
                {currentFilter}
              </span>
            );
          }) : ''
      }
    </div>
  );
};

const mapStateToProps = state => {
  const todo_items = state.todos.allIds.length;
  return {
    activeFilter: state.visibilityFilter,
    todo_items: todo_items
  };
};
// export default VisibilityFilters;
export default connect(
  mapStateToProps,
  { setFilter }
)(VisibilityFilters);
