import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO } from "../actionTypes";

const initialState = {
  allIds: [],
  byIds: {}
};
const removeWitoutMutation = (obj, prop) => {
  let res = Object.assign({}, obj)
  delete res[prop]
  return res
}
export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_TODO: {
      const { id, content } = action.payload;
      return {
        ...state,
        allIds: [...state.allIds, id],
        byIds: {
          ...state.byIds,
          [id]: {
            content,
            completed: false
          }
        }
      };
    }
    case REMOVE_TODO: {
      const { id } = action.payload;
      console.log("SARTU DA: ", state)
      return {
        ...state,
        allIds: state.allIds.filter(item => item !== id),
        byIds: removeWitoutMutation(state.byIds, id)
      };
    }
    case TOGGLE_TODO: {
      const { id } = action.payload;
      return {
        ...state,
        byIds: {
          ...state.byIds,
          [id]: {
            ...state.byIds[id],
            completed: !state.byIds[id].completed
          }
        }
      };
    }
    default:
      return state;
  }
}
